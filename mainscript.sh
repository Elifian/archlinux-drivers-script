# author: discord Elifian#0761
# Весь скрипт написан лично мной

videocard_nvidia=$(lspci | grep VGA | sed -rn 's/.*(NVIDIA).*/\1/p')
videocard_amd=$(lspci | grep VGA | sed -rn 's/.*(AMD).*/\1/p')
videocard_intel=$(lspci | grep VGA | sed -rn 's/.*(Intel).*/\1/p')

successfully_installed_nvidia(){
    _x_="$(tput setaf 2)[✗]$(tput sgr0)"
    _y_="$(tput setaf 1)[!]$(tput sgr0)"
    nvidia_version=$(modinfo $(find /usr/lib/modules -name nvidia.ko) | grep ^version | xargs | cut -c 10-)
    echo ""
    echo "$_x_ $(tput bold)Драйвера версии ${nvidia_version} успешно установлены.$(tput sgr0)"
    echo ""
    echo "$_x_ $(tput bold)Включен NVIDIA DRM.$(tput sgr0)"
    echo "      $(tput sitm)nvidia_drm включен в файле /etc/mkinitcpio.conf$(tput sgr0)"
    echo ""
    echo "$_y_ $(tput bold)Убедитесь, что у вас установлен пакет linux-headers для вашего ядра.$(tput sgr0)"
    echo "      $(tput sitm)Для проверки введите: pacman -Qs linux headers$(tput sgr0)"
    echo ""
    echo "$_y_ $(tput bold)Для полноценной работы не забудьте перезагрузится.$(tput sgr0)"
}

successfully_installed(){
    _x_="$(tput setaf 2)[✗]$(tput sgr0)"
    _y_="$(tput setaf 1)[!]$(tput sgr0)"
    echo ""
    echo "$_x_ $(tput bold)Драйвера успешно установлены.$(tput sgr0)"
    echo ""
    echo "$_y_ $(tput bold)Для полноценной работы не забудьте перезагрузится.$(tput sgr0)"
}

#https://nouveau.freedesktop.org/CodeNames.html
nvidia_serieses(){
    Tesla1=(G[0-9]*)
    Tesla2=(GT[0-9]*)
    Tesla3=(MCP[0-9]*)
    Fermi=(GF[0-9]*)
    Kepler=(GK[0-9]*)
    Maxwell=(GM[0-9]*)
    Pascal=(GP[0-9]*)
    Volta=(GV[0-9]*)
    Turing=(TU[0-9]*)
    Ampere=(GA[0-9]*)

    [[ $(lspci | grep VGA | sed -rn 's/.*(G[0-9]*).*/\1/p') == $Tesla1 ]] && NVIDIA=Tesla
    [[ $(lspci | grep VGA | sed -rn 's/.*(GT[0-9]*).*/\1/p') == $Tesla2 ]] && NVIDIA=Tesla
    [[ $(lspci | grep VGA | sed -rn 's/.*(MCP[0-9]*).*/\1/p') == $Tesla3 ]] && NVIDIA=Tesla
    [[ $(lspci | grep VGA | sed -rn 's/.*(GF[0-9]*).*/\1/p') == $Fermi ]] && NVIDIA=Fermi
    [[ $(lspci | grep VGA | sed -rn 's/.*(GK[0-9]*).*/\1/p') == $Kepler ]] && NVIDIA=Kepler
    [[ $(lspci | grep VGA | sed -rn 's/.*(GM[0-9]*).*/\1/p') == $Maxwell ]] && NVIDIA=Maxwell
    [[ $(lspci | grep VGA | sed -rn 's/.*(GP[0-9]*).*/\1/p') == $Pascal ]] && NVIDIA=Pascal
    [[ $(lspci | grep VGA | sed -rn 's/.*(GV[0-9]*).*/\1/p') == $Volta ]] && NVIDIA=Volta
    [[ $(lspci | grep VGA | sed -rn 's/.*(TU[0-9]*).*/\1/p') == $Turing ]] && NVIDIA=Turing
    [[ $(lspci | grep VGA | sed -rn 's/.*(GA[0-9]*).*/\1/p') == $Ampere ]] && NVIDIA=Ampere
}

NVIDIA_DRM_MKINITCPIO(){
if $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | sed -rn 's/.*(nvidia-drm.modeset=1).*/\1/p' | grep "nvidia-drm.modeset=1"); then
    :
else
    nvidia_parameters=("nvidia" "nvidia_modeset" "nvidia_uvm" "nvidia_drm")

    for param in "${nvidia_parameters[@]}"; do
        if [[ $(grep -v "#" /etc/mkinitcpio.conf | grep "MODULES=(.*)" | grep $param) ]]; then
            :
        elif [[ $(grep -v "#" /etc/mkinitcpio.conf | grep "MODULES=(.*)" | grep $param) != $param ]]; then
            sudo sed -i 's|MODULES=([^)]*|& '"$param"'|' /etc/mkinitcpio.conf
        fi
    done
fi
}

ARCHLINUX(){
    sudo pacman -Sy --noconfirm

    nvidia_340="$install_var -S --needed --noconfirm nvidia-340xx-dkms nvidia-340xx-utils nvidia-340xx-settings"
    nvidia_390="$install_var -S --needed --noconfirm nvidia-390xx-dkms nvidia-390xx-utils lib32-nvidia-390xx-utils nvidia-390xx-settings vulkan-icd-loader lib32-vulkan-icd-loader lib32-opencl-nvidia-390xx libxnvctrl-390xx"
    nvidia_470="$install_var -S --needed --noconfirm nvidia-470xx-dkms nvidia-470xx-utils lib32-nvidia-470xx-utils nvidia-470xx-settings vulkan-icd-loader lib32-vulkan-icd-loader lib32-opencl-nvidia-470xx opencl-nvidia-470xx libxnvctrl-470xx"
    nvidia_last="$install_var -S --needed --noconfirm nvidia-dkms nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader lib32-opencl-nvidia opencl-nvidia libxnvctrl"
    amd_driver="$install_var -S --needed --noconfirm mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader"
    intel_driver="$install_var -S --needed --noconfirm f86-video-intel libva libva-utils libva-intel-driver vulkan-intel lib32-libva lib32-libva-intel-driver mesa lib32-mesa vulkan-intel lib32-vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader"
}

if [[ -f /usr/bin/yay ]]; then
    install_var="yay"
    ARCHLINUX
fi
if [[ -f /usr/bin/pikaur ]]; then
    install_var="pikaur"
    ARCHLINUX
fi
if [[ -f /usr/bin/pakku ]]; then
    install_var="pakku"
    ARCHLINUX
fi
if [[ -f /usr/bin/trizen ]]; then
    install_var="trizen"
    ARCHLINUX
fi
if [[ -f /usr/bin/paru ]]; then
    install_var="paru"
    ARCHLINUX
fi

if [[ $install_var == "" && -f /usr/bin/pacman ]]; then
    if grep "chaotic-aur" /etc/pacman.conf >/dev/null; then
    	:
    else
        sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
        sudo pacman-key --lsign-key FBA220DFC880C036
        sudo pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
        sudo sed -i '$ a \\n[chaotic-aur]\nInclude = /etc/pacman.d/chaotic-mirrorlist' /etc/pacman.conf
    fi
    install_var="sudo pacman"
    ARCHLINUX
fi

nvidia_series_check(){
    if [[ $NVIDIA == "" ]]; then
        echo "$(tput setaf 1)$(tput bold)Ваша видеокарта не поддерживает проприетарные драйвера$(tput sgr0)"
    else
        if [[ $NVIDIA == Tesla ]]; then
            $nvidia_340
        elif [[ $NVIDIA == Fermi ]]; then
            $nvidia_390
        elif [[ $NVIDIA == Kepler ]]; then
            $nvidia_470
        elif [[ $NVIDIA == Maxwell || $NVIDIA == Pascal || $NVIDIA == Volta || $NVIDIA == Turing || $NVIDIA == Ampere ]]; then
            $nvidia_last
        fi
        NVIDIA_DRM_MKINITCPIO
        successfully_installed_nvidia
    fi
}

if [[ $videocard_nvidia == "NVIDIA" ]]; then
    nvidia_serieses
    nvidia_series_check
elif [[ $videocard_amd == "AMD" ]]; then
    amd_driver
    successfully_installed
elif [[ $videocard_intel == "Intel" ]]; then
    intel_driver
    successfully_installed
else
    echo "$(tput setaf 1)$(tput bold)Ваша видеокарта не определена, установка драйвера отменена$(tput sgr0)"
fi
